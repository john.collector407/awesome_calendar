import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:weird_calendar/src/default_configs.dart';

import '../settings/settings_view.dart';

class CalendarView extends StatefulWidget {
  const CalendarView({super.key});

  @override
  State<CalendarView> createState() => _CalendarViewState();
}

class _CalendarViewState extends State<CalendarView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xffF0F3FF),
        elevation: 0.1,
        actions: [
          IconButton(
            icon: const Icon(Icons.search, color:  Color(0xff00000F)),
            onPressed: () {
              Navigator.restorablePushNamed(context, SettingsView.routeName);
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          // mainAxisSize: MainAxisSize.min,
          children: [
            // const TextField(
            //   decoration: InputDecoration(
            //     alignLabelWithHint: true,
            //     labelText: 'Title',
            //     floatingLabelBehavior: FloatingLabelBehavior.never,
            //     border: UnderlineInputBorder(
            //       borderSide: BorderSide(
            //         color: Color(0xff6784DD),
            //       )
            //     )
            //   ),
            // ),
            // DropdownButton(
              // items: [], onChanged: (){
              // }),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: const Color(0xffF0F3FF),
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: TableCalendar(
                    focusedDay: DateTime.now(),
                    firstDay: startDay,
                    lastDay: lastDay,
                    calendarFormat: CalendarFormat.week,
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}